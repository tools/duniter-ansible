# Duniter Version 2 Ansible Role

This repository propose an Ansible role to install and maintain Duniter Version 2 nodes.

The code is licenced under the free software GPL v3 licence. See COPYING file.

## Controller Station Requirements

Python3 is required. Tested with Python 3.10.

This repository should be used in a Python virtual environment.

Install Python requirements with:

    pip3 install -U -r requirements.txt

## Hosts requirements

Hosts should be able to execute Ansible playbooks so they should have installed:
* Python3
* SSH server
* Docker

## Configuration

Each node can be configured with ansible variables. Defaults values should be as follow:

```yaml
####################################
# duniter docker compose vars

# duniter docker compose state
duniter_docker_compose_state: present

# duniter docker compose volume list
duniter_docker_compose_volumes:
  data:

# duniter docker compose network list
duniter_docker_compose_networks:
  app_net:
    driver: bridge
    ipam:
      driver: default
      config:
        - subnet: 172.16.238.0/24

####################################
# duniter services vars

# duniter docker image
duniter_docker_image: "duniter/duniter-v2s:debug-latest"

# duniter docker restart policy
duniter_docker_restart: unless-stopped

# duniter docker ports
duniter_docker_ports:
  # telemetry
  - "127.0.0.1:9615:9615"
  # rpc
  - "127.0.0.1:9933:9933"
  # rpc-ws
  - "127.0.0.1:9944:9944"
  # p2p
  - "30333:30333"

# duniter docker environment variables
duniter_docker_env:
  DUNITER_INSTANCE_NAME: "my_instance"
  DUNITER_CHAIN_NAME: "local"
  #DUNITER_DISABLE_PROMETHEUS: "false"

# duniter docker command options
duniter_docker_command: ["--alice", "--node-key", "0000000000000000000000000000000000000000000000000000000000000001"]

# duniter docker volumes
duniter_docker_volumes:
  - data:/var/lib/duniter

# duniter_v2s docker networks
duniter_docker_networks:
  app_net:
    ipv4_address: 172.16.238.10
####################################
```

# Run the playbooks

    ansible-playbooks --inventory inventory/node.yml playbooks/duniter2.yml
